package ui;

import Pages.GoogleSearchPage;
import org.testng.annotations.Test;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

public class GoogleTest extends BaseUITest {

    private final String IMAGE_URL = "https://m.atcdn.co.uk/ect/media/w1024/brand-store/volkswagen/golf/hero.jpg";

    @Test
    public void googleTest() {
        GoogleSearchPage googleSearchPage = new GoogleSearchPage();

        String[] relatedSearchKeywords =
                googleSearchPage
                        .searchImage()
                        .clickOnSearchByImageIcon()
                        .inputImageURL(IMAGE_URL)
                        .getRelatedSearchKeywords();

        String pageContent = googleSearchPage
                .clickOnResult(result)
                .getPageContent();

        assertThat(Arrays.stream(relatedSearchKeywords).anyMatch(relatedWord -> pageContent.contains(relatedWord)))
                .as("Check if result page contains any keywords suggested by google for the image")
                .isTrue();
    }
}
