package ui;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Allure;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.*;

import java.io.ByteArrayInputStream;

import static com.codeborne.selenide.Selenide.open;

public class BaseUITest {

    protected int result;

    @BeforeSuite
    @Parameters({"result"})
    public void setupDriver(@Optional("1") String result_param) {
        this.result = Integer.parseInt(result_param);
        WebDriverManager.chromedriver().setup();
        Configuration.startMaximized = true;
        open("https://google.com");
    }

    @AfterMethod
    public void screenshot() {
        Allure.addAttachment("Page: ",
                new ByteArrayInputStream(((TakesScreenshot) WebDriverRunner.getWebDriver()).getScreenshotAs(OutputType.BYTES)));
    }

    @AfterSuite(alwaysRun = true)
    public void shutdown() {
        Selenide.closeWebDriver();
    }
}
