package Pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;

public class GoogleSearchPage {

    private final SelenideElement agreeButton = $x("//div[text()='I agree']");
    private final SelenideElement searchImagesLink = $x("//a[contains(.,'Images')]");
    private final SelenideElement searchImageIcon = $x("//div[@aria-label='Search by image']");
    private final SelenideElement searchInput = $x("//input[@name='image_url']");
    private final SelenideElement submintSearch = $x("//input[@type='submit']");
    private final ElementsCollection searchResults = $$x("//div[@class='normal-header']/following-sibling::div");
    private final SelenideElement relatedSearch = $x("//div[@id='topstuff']//div[contains(.,'Possible related search:')]/a");
    private final SelenideElement annoyingMessage = $(byText("Before you continue to Google Search"));

    @Step
    public GoogleSearchPage searchImage() {
        dismissAnnoyingMessage();
        searchImagesLink.click();
        return this;
    }

    @Step
    public GoogleSearchPage clickOnSearchByImageIcon() {
        dismissAnnoyingMessage();
        searchImageIcon.click();
        return this;
    }

    @Step
    public GoogleSearchPage inputImageURL(String image_url) {
        searchInput.val(image_url);
        submintSearch.click();
        return this;
    }

    @Step("Picking result #{0}")
    public ResultPage clickOnResult(int result) {
        searchResults.get(result - 1).$x("descendant::h3").click();
        return new ResultPage();
    }

    @Step("Get related keywords from search")
    public String[] getRelatedSearchKeywords() {
        return relatedSearch.getText().split(" ");
    }

    private void dismissAnnoyingMessage() {
        if (annoyingMessage.is(visible)) {
            agreeButton.click();
            annoyingMessage.shouldNotBe(visible);
        }
    }
}
