package Pages;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Selenide.$x;

public class ResultPage {

    private final SelenideElement pageBody = $x("//body");

    @Step
    public String getPageContent() {
        return pageBody.text();
    }
}
