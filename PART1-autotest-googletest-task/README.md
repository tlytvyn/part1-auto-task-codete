## Running tests

In most simple way this command could be used to start tests execution:

```
$ mvn clean test
```

## Allure report

Once tests were executed `tests` module will contain `allure-results` directory. Content of that directory is used for Allure report generating.

In order to generate report after run use `mvn allure:report`. Once command is completed `allure-report` directory is created in project root. You can observe the report by opening `allure-report\index.html` in web browser.

Also, `mvn allure:serve` could be used for immediate opening it in your default web browser. Please note that in this case report is hosted through started jetty server.

There is sample report in `allure-report` directory created.

## Test execution

mvn clean test -Dresult=3

Where `-Dresult=3` is the number of result you want to check